FROM node:carbon

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN npm install --production

COPY . .

RUN npm run build

RUN npm install --g serve

EXPOSE 3000

CMD [ "serve", "-s", "build" ]
