import axios from 'axios';
import axiosRetry from 'axios-retry';

export default class Request {
  constructor() {
    const { MAX_RETRIES, TIMEOUT, BASE_URL } = {
      MAX_RETRIES: 3,
      TIMEOUT: 30000,
      BASE_URL: 'http://localhost:3001'
    };

    const defaultConfig = {
      timeout: TIMEOUT,
      baseURL: BASE_URL,
    };

    this.axiosRequest = axios.create(defaultConfig);
    axiosRetry(this.axiosRequest, { retries: MAX_RETRIES });
  }

  get = async (...args) => await this.axiosRequest.get(...args);

  post = async (...args) => await this.axiosRequest.post(...args);
}
