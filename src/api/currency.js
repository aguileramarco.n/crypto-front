import Base from './base';

class Currency extends Base {
  getFunctionOptions = () => this.get('/functionOptions');
  getSymbolOptions = () => this.get('/symbolOptions');
  getMarketOptions = () => this.get('/marketOptions');
  getCurrencyHistory = async (query) => await this.get('/currencyHistory', query);
}

export default Currency;
