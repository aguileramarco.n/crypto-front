import React, { Component } from 'react';

import '../styles/Home.css';

class Home extends Component {
  render() {
    return (
      <div className="App">
        <div className="home">
          <div className="home-header">
            <h2>Welcome to React Dashboard</h2>
          </div>
          <p className="home-intro">
            {'<<<<<'} Please select an options to show the currency history
          </p>
        </div>
      </div>
    );
  }
}

export default Home;
