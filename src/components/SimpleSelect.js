import React from 'react';
import { SelectField } from 'react-md';

const SimpleSelect = ({ value, stringItems, placeholder, onChange, simplifiedMenu = true, disabled = false }) => {
  const randomId = Math.round(Math.random()*10000);

  return (
    <SelectField
      id={`select-field-${randomId}`}
      placeholder={placeholder}
      className="md-cell"
      menuItems={stringItems}
      simplifiedMenu={simplifiedMenu}
      onChange={onChange}
      value={value}
      disabled={disabled}
    />
  );
};

export default SimpleSelect;