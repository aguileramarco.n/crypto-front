import React from 'react';
import { DatePicker } from 'react-md';

const FilterDate = ({ label, defaultValue, onChange }) => {
  const randomId = Math.round(Math.random()*10000);

  return (
    <DatePicker
      id={`date-picker-${randomId}`}
      inline
      displayMode="portrait"
      label={label}
      onChange={onChange}
      fullWidth={false}
      defaultValue={defaultValue}
      autoOk={true}
    />
  );
};
export default FilterDate;
