import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';
import moment from 'moment';
import SimpleSelect from './SimpleSelect';
import LoadingSpinner from './LoadingSpinner';
import FilterDate from './FilterDate';
import { Currency } from '../api';

const DATE_NOW = moment().format('DD/MM/YYYY');
const DEFAULT_PERIOD = 'DIGITAL_CURRENCY_MONTHLY';
const DEFAULT_SYMBOL = 'BTC';
const DEFAULT_MARKET = 'USD';

class MonthChart extends Component {
  constructor(){
    super();
    this.state = {
      functions: DEFAULT_PERIOD,
      symbol: DEFAULT_SYMBOL,
      market: DEFAULT_MARKET,
      functionsOptions: [],
      symbolOptions: [],
      marketOptions: [],
      currencyHistory: [],
      isLoading: false,
      dateMin: DATE_NOW,
      dateMax: DATE_NOW,
    }
  }

  componentDidMount() {
    this.searchData();
    this.searchHistory();
  }

  setMinDate = (selectedDate) => {
    this.setState({dateMin: selectedDate}, this.searchHistory);
  }

  setMaxDate = (selectedDate) => {
    this.setState({dateMax: selectedDate}, this.searchHistory);
  }

  selectFunctions = (selected) => {
    this.setState({functions: selected}, this.searchHistory);
  }
  
  selectSymbol = (selected) => {
    this.setState({symbol: selected}, this.searchHistory);
  }

  selectMarket = (selected) => {
    this.setState({market: selected}, this.searchHistory);
  }

  render() {
    const { functionsOptions, symbolOptions, marketOptions } = this.state;
    const { currencyHistory, isLoading } = this.state;

    return (
      <div className="md-grid">
        <h2 className="md-cell md-cell--12 md-text-container">
          MONTHLY HISTORY
        </h2>

        <h4 className="md-cell md-cell--12">Filters to get Data</h4>
        <FilterDate
          defaultValue={new Date()}
          label={'Select MIN date'}
          onChange={this.setMinDate}
        />
        <FilterDate
          defaultValue={new Date()}
          label={'Select MAX date'}
          onChange={this.setMaxDate}
        />

        <div className="md-cell md-cell--12">
          <SimpleSelect
            value={DEFAULT_PERIOD}
            stringItems={functionsOptions}
            placeholder={'Functions list'}
            simplifiedMenu={true}
            onChange={this.selectFunctions}
            disabled={true}
          />
          <SimpleSelect
            stringItems={symbolOptions}
            placeholder={'Symbol list'}
            simplifiedMenu={true}
            onChange={this.selectSymbol}
          />
          <SimpleSelect
            stringItems={marketOptions}
            placeholder={'Market list'}
            simplifiedMenu={true}
            onChange={this.selectMarket}
          />

          {isLoading ? <LoadingSpinner /> : <LineChart data={currencyHistory}/>}
        </div>
      </div>

    );
  }

  searchData(){
    Currency.getFunctionOptions().then(res => {
      this.setState({functionsOptions: res.data});
    })
    Currency.getSymbolOptions().then(res => {
      this.setState({symbolOptions: res.data});
    })
    Currency.getMarketOptions().then(res => {
      this.setState({marketOptions: res.data});
    })
  }

  searchHistory() {
    this.setState({isLoading: true});

    const { functions, symbol, market } = this.state;
    const { dateMin, dateMax } = this.state;
    
    const query = {
      params:{
        functions,
        symbol,
        market,
        dateMin,
        dateMax,
      }
    };
    Currency.getCurrencyHistory(query).then(res => {
      let obj = {};
      res.data.forEach(d => {
        return Object.assign(obj, d);
      })
      const dataChart = [{
        name: this.state.symbol,
        data: obj,
      }];
      this.setState({isLoading: false});
      this.setState({currencyHistory: dataChart});
    }).catch(err => {
      this.setState({isLoading: false});
      this.setState({currencyHistory: []});
    })
  };
}

export default MonthChart;
