import React from 'react';

const LoadingSpinner = () => (
  <div>
    Loading data...
  </div>
);

export default LoadingSpinner;
