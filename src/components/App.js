import React, { Component } from 'react';
import { NavigationDrawer } from 'react-md';
import { Route, Switch } from 'react-router-dom';

import NavLink from './NavLink';

import Home from './Home';
import HourChart from './HourChart';
import MonthChart from './MonthChart';

const navItems = [{
  exact: true,
  label: 'Home',
  to: '/',
  icon: 'home',
}, {
  label: 'Hourly Chart',
  to: '/hour',
  icon: 'bookmark',
}, {
  label: 'Monthly Chart',
  to: '/month',
  icon: 'donut_large',
}];

class App extends Component {
  render() {
    return (
      <Route
        render={({ location }) => (
          <NavigationDrawer
            drawerTitle="Options"
            toolbarTitle="Currency BTC & ETH history"
            navItems={navItems.map(props => <NavLink {...props} key={props.to} />)}
          >
            <Switch key={location.key}>
              <Route exact path="/" location={location} component={Home} />
              <Route path="/hour" location={location} component={HourChart} />
              <Route path="/month" location={location} component={MonthChart} />
            </Switch>
          </NavigationDrawer>
        )}
      />
    );
  }
}

export default App;
