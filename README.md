# crypto-front
------------

### Node

[Node](http://nodejs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    >=v9.10

    $ npm --version
    6.1.0

## Install

    $ git clone https://gitlab.com/aguileramarco.n/crypto-front.git
    $ cd crypto-front

## Install dependencies


    $ npm install --production
`or`

    $ yarn

## Starting Local

To start project

    $ npm run start
`or`

    $ yarn start

    $ yarn dev

## Starting with Docker-compose
------------
Before to start this project ensure the `backend` is running correctly
To build this project (take some minutes...)

    $ docker-compose build

To start project

    $ docker-compose up -d

To delete project

    $ docker-compose rm

## Is running
------------

Open web browser

    $ http://localhost:3000/

    $ http://localhost:3000/hour

    $ http://localhost:3000/month

#### Note: If an invalid date range is selected, then default values are returned (24hrs, 12months)

## Languages & tools

### JavaScript

- [React](https://reactjs.org/) is used for UI.
- [Axios](https://www.npmjs.com/package/axios) is used for API Request.
- [Chartkick](https://www.chartkick.com/) is used for Charts in views.
- [Moment.js](https://momentjs.com/) is used for filter data by datetime.
- [node-sass-chokidar](https://reactjs.org/) it allows to compile .scss files to css.
